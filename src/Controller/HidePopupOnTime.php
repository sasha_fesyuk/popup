<?php

namespace Drupal\popup\Controller;

use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Controller\ControllerBase;

/**
 * Controller to hide popup message on some time.
 */
class HidePopupOnTime extends ControllerBase {

  /**
   * Set a big live cache time.
   *
   * @param int $time
   *   Time of living of cache.
   */
  public function setCache($time) {
    // New response.
    $response = new AjaxResponse();

    \Drupal::cache()->set(
      'popup_cache_time',
      \Drupal::time()->getRequestTime() + $time
    );

    // Return response.
    return $response;
  }

}
