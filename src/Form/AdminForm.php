<?php

namespace Drupal\popup\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Config admin form.
 */
class AdminForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'popup_admin_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'popup.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('popup.settings');

    if (!empty($config->get('enable_popup'))) {
      $enable_popup = $config->get('enable_popup');
    }
    else {
      $enable_popup = FALSE;
    }

    $form['enable_popup'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable popup:'),
      '#default_value' => $enable_popup,
    ];

    $form['title'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Title'),
      '#description' => $this->t('Enter the title of popup message (more than 3 symbols).'),
      '#default_value' => $config->get('title'),
      '#required' => TRUE,
    ];

    $format = 'restricted_html';
    if (!empty($config->get('text_of_message.format'))) {
      $format = $config->get('text_of_message.format');
    }

    $form['text_of_message'] = [
      '#type' => 'text_format',
      '#title' => $this->t('Text of popup message'),
      '#description' => $this->t('Enter text of popup message (more than 100 symbols).'),
      '#default_value' => $config->get('text_of_message.value'),
      '#format' => $format,
      '#required' => TRUE,
    ];

    $cache_time = 0;
    if (!empty($config->get('cache_time'))) {
      $cache_time = $config->get('cache_time');
    }

    $form['cache_time'] = [
      '#type' => 'number',
      '#title' => $this->t('Live cache time'),
      '#required' => TRUE,
      '#description' => $this->t('Time of living of cache.'),
      '#max' => 604800,
      '#min' => 0,
      '#default_value' => $cache_time,
    ];

    // In checkbox not checked.
    if (!$enable_popup) {
      // Block next fields.
      $form['title']['#attributes'] = ['disabled' => 'disabled'];
      $form['text_of_message']['#attributes'] = ['disabled' => 'disabled'];
      $form['cache_time']['#attributes'] = ['disabled' => 'disabled'];
    }

    // Attache js to the admin form.
    $form['#attached']['library'][] = 'popup/popup.admin.form.js';

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {
    if (strlen($form_state->getValue('title')) < 3) {
      $form_state->setErrorByName(
        'title',
        $this->t('The title is too short.')
      );
    }

    if (strlen($form_state->getValue('text_of_message')['value']) < 100) {
      $form_state->setErrorByName(
        'text_of_message',
        $this->t('The text of message is too short.')
      );
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('popup.settings')
      ->set('enable_popup', $form_state->getValue('enable_popup'))
      ->set('title', $form_state->getValue('title'))
      ->set('text_of_message', $form_state->getValue('text_of_message'))
      ->set('cache_time', $form_state->getValue('cache_time'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
