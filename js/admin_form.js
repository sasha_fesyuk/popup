/**
 * @file
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.popup_admin_form_js = {
    attach: function (context, settings) {

      // Get form.
      var $admin_form = $('.popup-admin-form').once('popup_admin_form_js');
      // Get checkbox.
      var $checkbox = $admin_form.find('.form-checkbox');

      $checkbox.on('click', function () {
        // Check from the false to the true.
        if (this.checked) {
          // Enable fields of form.
          $admin_form.find('.form-item-title')
            .removeClass('form-disabled')
            .find('input').removeAttr('disabled');

          $admin_form.find('.form-item-text-of-message-value')
            .removeClass('form-disabled')
            .find('textarea').removeAttr('disabled');

          $admin_form.find('.form-item-cache-time')
            .removeClass('form-disabled')
            .find('input').removeAttr('disabled');
        }
        // Check from the true to the false.
        else {
          // Disable fields of form.
          $admin_form.find('.form-item-title')
            .addClass('form-disabled')
            .find('input').attr('disabled','disabled');

          $admin_form.find('.form-item-text-of-message-value')
            .addClass('form-disabled')
            .find('textarea').attr('disabled','disabled');

          $admin_form.find('.form-item-cache-time')
            .addClass('form-disabled')
            .find('input').attr('disabled','disabled');
        }
      });
    }
  }

}(jQuery, Drupal, drupalSettings));
