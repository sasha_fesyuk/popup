(function ($, Drupal, drupalSettings) {

  'use strict';

  Drupal.behaviors.popup_js = {
    attach: function (context, settings) {

      // Get data from drupalSettings.
      var title = drupalSettings.title;
      var text_of_message = drupalSettings.text_of_message;

      // Show popup window.
      var popup_window
        = Drupal.dialog('<div>' + text_of_message + '</div>', {
        title: title,
        dialogClass: 'front-modal',
        width: 500,
        height: 400,
        autoResize: true,
        close: function (event) {
          $(event.target).remove();
        },
        buttons: [
          {
            text: Drupal.t('Do not show again'),
            click: function () {
              // Hide popup for next week.
              $.ajax({url: '/hide-popup/604800'});
              // Close window.
              $(this).dialog('close');
            }
          },
          {
            text: Drupal.t('Close the window'),
            icons: {
              primary: 'ui-icon-close'
            },
            click: function () {
              $(this).dialog('close');
            }
          }
        ]
      });
      popup_window.showModal();
    }
  }

}(jQuery, Drupal, drupalSettings));
